# Nvidia driver installer for Arch Linux

[Master-Branch](https://gitlab.com/XavierEduardo99/nvidia-drivers-arch-linux-installer/-/tree/main)

This bash script will install Xorg and the Nvidia drivers (LTS & regular) for Arch Linux, and will notify the user if any error is encountered.

## Dependencies:
In order to run this project you will need the following packages
* pacman
* sudo or doas

## Usage guide: 

+ To install Nvidia drivers that support the latest kernel, run:
```sh
$ sudo install-nvidia.sh
```

+ To install Nvidia drivers that support the LTS kernel, run:
```sh
$ sudo install-nvidia.sh --lts
```

## Contact with the developer
| Contact | Link |
| ------ | ------ |
| My Mail | xaviervintimilla@yahoo.es |
